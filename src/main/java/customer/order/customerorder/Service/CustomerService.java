package customer.order.customerorder.Service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import customer.order.customerorder.Model.Customer;
import customer.order.customerorder.Model.Order;
import customer.order.customerorder.Repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository iCustomerRepository;
    
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> lCustomers = new ArrayList<>();

        iCustomerRepository.findAll().forEach(lCustomers::add);

        return lCustomers;
    }

    public Set<Order> getOrderByCustomerId(long customer_id){
        Customer vCustomer = iCustomerRepository.findById(customer_id);
        if (vCustomer != null) {
            return vCustomer.getOrders();
        } else {
            return null;
        }
    }
}
