package customer.order.customerorder.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import customer.order.customerorder.Model.Order;
import customer.order.customerorder.Repository.OrderRepository;

@Service
public class OrderService {
    @Autowired
    OrderRepository iOrderRepository;

    public ArrayList<Order> getAllOrders(){
        ArrayList<Order> lOrders = new ArrayList<>();
        iOrderRepository.findAll().forEach(lOrders::add);
        return lOrders;
    }
}
