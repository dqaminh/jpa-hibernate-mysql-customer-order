package customer.order.customerorder.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import customer.order.customerorder.Model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    
}
