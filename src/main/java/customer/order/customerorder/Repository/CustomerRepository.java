package customer.order.customerorder.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import customer.order.customerorder.Model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findById(long customer_id);
}
